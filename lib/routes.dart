import 'package:auto_route/auto_route.dart';
import 'package:navigation_auto_route_tutorial/pages/general/no_internet_connection/no_internet_connection.dart';
import 'package:navigation_auto_route_tutorial/pages/general/terms/terms.dart';
import 'package:navigation_auto_route_tutorial/pages/login/phone_login/phone_login.dart';
import 'package:navigation_auto_route_tutorial/pages/login/verificaton_code/verification_code.dart';
import 'package:navigation_auto_route_tutorial/pages/transactions/about_us/about_us.dart';
import 'package:navigation_auto_route_tutorial/pages/transactions/all_transactions/all_transactions.dart';
import 'package:navigation_auto_route_tutorial/pages/transactions/balance/balance.dart';
import 'package:navigation_auto_route_tutorial/pages/transactions/main_page.dart';

import 'group_screens/tab1_screen.dart';
import 'group_screens/group_screen.dart';
import 'group_screens/tab2_screen.dart';
import 'group_screens/tab3_screen.dart';
import 'login_screens/login_screen.dart';
import 'login_screens/signup_screen.dart';

@AdaptiveAutoRouter(
  replaceInRouteName: 'Page,Route,Screen',
  routes: <AutoRoute>[
    //authentification routes
    AutoRoute(
      initial: true,
      path: '/terms',
      page: Terms,
      children: [
        RedirectRoute(path: '*', redirectTo: ''),
      ],
    ),
    AutoRoute(
      path: '/no_internet_connection',
      page: NoInternetConnection,
      children: [
        RedirectRoute(path: '*', redirectTo: ''),
      ],
    ),
    groupTabRouter,

    // //user routes with a nested router
    // AutoRoute(
    //   path: '/user',
    //   page: UserScreen,
    //   children: [
    //     AutoRoute(path: '', page: UserProfileScreen),
    //     AutoRoute(path: 'details/*', page: UserDetailsScreen),
    //     AutoRoute(path: 'friends/*', page: UserFriendsScreen),
    //     groupTabRouter,
    //     // redirect all other paths
    //     RedirectRoute(path: '*', redirectTo: ''),
    //   ],
    // ),

    // redirect all other paths
    RedirectRoute(path: '*', redirectTo: '/terms'),
    //Home
  ],
)
class $AppRouter {}

//nested group route with a tab router
const groupTabRouter = AutoRoute(
  path: 'group/:id',
  page: GroupScreen,
  children: [
    AutoRoute(
      path: 'login',
      name: 'LoginScreen',
      page: EmptyRouterPage,
      children: [
        AutoRoute(path: '', page: PhoneLogin),
        AutoRoute(path: 'verification_code', page: VerificationCode),
        RedirectRoute(path: '*', redirectTo: ''),
      ],
    ),
    AutoRoute(
      path: 'main_page',
      name: 'MainPageScreen',
      page: MainPage,
      children: [
        AutoRoute(path: 'about_us', page: AboutUs),
        AutoRoute(path: '', page: AllTransactions),
        AutoRoute(path: 'balance', page: Balance),
        RedirectRoute(path: '*', redirectTo: ''),
      ],
    ),
    RedirectRoute(path: '*', redirectTo: 'main_page'),
  ],
  // children: [
  //   AutoRoute(
  //     path: 'tab1',
  //     name: 'GroupTab1Router',
  //     page: EmptyRouterPage,
  //     children: [
  //       AutoRoute(path: '', page: Tab1Screen),
  //       RedirectRoute(path: '*', redirectTo: ''),
  //     ],
  //   ),
  //   AutoRoute(
  //     path: 'tab2',
  //     name: 'GroupTab2Router',
  //     page: EmptyRouterPage,
  //     children: [
  //       AutoRoute(path: '', page: Tab2Screen),
  //       RedirectRoute(path: '*', redirectTo: ''),
  //     ],
  //   ),
  //   AutoRoute(
  //     path: 'tab3',
  //     name: 'GroupTab3Router',
  //     page: EmptyRouterPage,
  //     children: [
  //       AutoRoute(path: '', page: Tab3Screen),
  //       RedirectRoute(path: '*', redirectTo: ''),
  //     ],
  //   ),
  // ],
);
