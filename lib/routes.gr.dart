// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;
import 'package:navigation_auto_route_tutorial/group_screens/group_screen.dart'
    as _i5;
import 'package:navigation_auto_route_tutorial/pages/general/no_internet_connection/no_internet_connection.dart'
    as _i4;
import 'package:navigation_auto_route_tutorial/pages/general/terms/terms.dart'
    as _i3;
import 'package:navigation_auto_route_tutorial/pages/login/phone_login/phone_login.dart'
    as _i7;
import 'package:navigation_auto_route_tutorial/pages/login/verificaton_code/verification_code.dart'
    as _i8;
import 'package:navigation_auto_route_tutorial/pages/transactions/about_us/about_us.dart'
    as _i9;
import 'package:navigation_auto_route_tutorial/pages/transactions/all_transactions/all_transactions.dart'
    as _i10;
import 'package:navigation_auto_route_tutorial/pages/transactions/balance/balance.dart'
    as _i11;
import 'package:navigation_auto_route_tutorial/pages/transactions/main_page.dart'
    as _i6;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    TermsRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i3.Terms());
    },
    NoInternetConnectionRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i4.NoInternetConnection());
    },
    GroupScreenRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i5.GroupScreen());
    },
    LoginScreen.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i1.EmptyRouterPage());
    },
    MainPageScreen.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i6.MainPage());
    },
    PhoneLoginRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i7.PhoneLogin());
    },
    VerificationCodeRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i8.VerificationCode());
    },
    AboutUsRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i9.AboutUs());
    },
    AllTransactionsRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i10.AllTransactions());
    },
    BalanceRoute.name: (routeData) {
      return _i1.AdaptivePage<dynamic>(
          routeData: routeData, child: _i11.Balance());
    }
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig('/#redirect',
            path: '/', redirectTo: '/terms', fullMatch: true),
        _i1.RouteConfig(TermsRoute.name, path: '/terms', children: [
          _i1.RouteConfig('*#redirect',
              path: '*', redirectTo: '', fullMatch: true)
        ]),
        _i1.RouteConfig(NoInternetConnectionRoute.name,
            path: '/no_internet_connection',
            children: [
              _i1.RouteConfig('*#redirect',
                  path: '*', redirectTo: '', fullMatch: true)
            ]),
        _i1.RouteConfig(GroupScreenRoute.name, path: 'group/:id', children: [
          _i1.RouteConfig(LoginScreen.name, path: 'login', children: [
            _i1.RouteConfig(PhoneLoginRoute.name, path: ''),
            _i1.RouteConfig(VerificationCodeRoute.name,
                path: 'verification_code'),
            _i1.RouteConfig('*#redirect',
                path: '*', redirectTo: '', fullMatch: true)
          ]),
          _i1.RouteConfig(MainPageScreen.name, path: 'main_page', children: [
            _i1.RouteConfig(AboutUsRoute.name, path: 'about_us'),
            _i1.RouteConfig(AllTransactionsRoute.name, path: ''),
            _i1.RouteConfig(BalanceRoute.name, path: 'balance'),
            _i1.RouteConfig('*#redirect',
                path: '*', redirectTo: '', fullMatch: true)
          ]),
          _i1.RouteConfig('*#redirect',
              path: '*', redirectTo: 'main_page', fullMatch: true)
        ]),
        _i1.RouteConfig('*#redirect',
            path: '*', redirectTo: '/terms', fullMatch: true)
      ];
}

class TermsRoute extends _i1.PageRouteInfo {
  const TermsRoute({List<_i1.PageRouteInfo>? children})
      : super(name, path: '/terms', children: children);

  static const String name = 'TermsRoute';
}

class NoInternetConnectionRoute extends _i1.PageRouteInfo {
  const NoInternetConnectionRoute({List<_i1.PageRouteInfo>? children})
      : super(name, path: '/no_internet_connection', children: children);

  static const String name = 'NoInternetConnectionRoute';
}

class GroupScreenRoute extends _i1.PageRouteInfo {
  const GroupScreenRoute({List<_i1.PageRouteInfo>? children})
      : super(name, path: 'group/:id', children: children);

  static const String name = 'GroupScreenRoute';
}

class LoginScreen extends _i1.PageRouteInfo {
  const LoginScreen({List<_i1.PageRouteInfo>? children})
      : super(name, path: 'login', children: children);

  static const String name = 'LoginScreen';
}

class MainPageScreen extends _i1.PageRouteInfo {
  const MainPageScreen({List<_i1.PageRouteInfo>? children})
      : super(name, path: 'main_page', children: children);

  static const String name = 'MainPageScreen';
}

class PhoneLoginRoute extends _i1.PageRouteInfo {
  const PhoneLoginRoute() : super(name, path: '');

  static const String name = 'PhoneLoginRoute';
}

class VerificationCodeRoute extends _i1.PageRouteInfo {
  const VerificationCodeRoute() : super(name, path: 'verification_code');

  static const String name = 'VerificationCodeRoute';
}

class AboutUsRoute extends _i1.PageRouteInfo {
  const AboutUsRoute() : super(name, path: 'about_us');

  static const String name = 'AboutUsRoute';
}

class AllTransactionsRoute extends _i1.PageRouteInfo {
  const AllTransactionsRoute() : super(name, path: '');

  static const String name = 'AllTransactionsRoute';
}

class BalanceRoute extends _i1.PageRouteInfo {
  const BalanceRoute() : super(name, path: 'balance');

  static const String name = 'BalanceRoute';
}
