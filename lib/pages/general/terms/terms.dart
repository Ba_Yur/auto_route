import 'package:flutter/material.dart';
import 'package:navigation_auto_route_tutorial/group_screens/group_screen.dart';
import 'package:navigation_auto_route_tutorial/routes.gr.dart';
import 'package:auto_route/auto_route.dart';

class Terms extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Center(
            child: Text('Terms'),
          ),
          ElevatedButton(onPressed: (){
            context.router.replace(GroupScreenRoute());
          }, child: Text('GroupScreen'))
        ],
      ),
    );
  }
}